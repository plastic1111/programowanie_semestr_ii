#ifndef KARTA_H
#define KARTA_H


class karta
{
    public: //wszystko od momentu public bedzie dostepne dla uzytkownikow programu np dla funkcji.
        karta();//konstruktor - metoda wywolywana w momencie stworzenia naszego obiektu.
        void test(int); // funkcja pobierajaca
        virtual ~karta();
    protected: //obiekt kotry dziedziczy po naszym typie moze nadpisac te funkcje i moze je odczytac.
    private: // jest dostepne tylko dla tej classy, dostep tylko za pomocom metod uzywanych w klasie.
};

#endif // KARTA_H
