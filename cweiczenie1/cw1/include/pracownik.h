#ifndef PRACOWNIK_H
#define PRACOWNIK_H
#include "karta.h"
class pracownik:public karta
{
    public: //wszystko od momentu public bedzie dostepne dla uzytkownikow programu np dla funkcji.
    void test(int)
    {
        std::cout << "pracownik/test" << std::endl;
    }
    pracownik() //konstruktor - metoda wywolywana w momencie stworzenia naszego obiektu.
    {
        std::cout << "pracownik/konstruktor" << std::endl; //ctor
    }
    virtual ~pracownik()
    {
        std::cout << "pracownik/destruktor" << std::endl; //dtor
    }
    protected: //obiekt kotry dziedziczy po naszym typie moze nadpisac te funkcje i moze je odczytac.
    private: // jest dostepne tylko dla tej classy, dostep tylko za pomocom metod uzywanych w klasie.
};



#endif // PRACOWNIK_H
